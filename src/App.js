import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import Chat from './components/Chat/Chat';
import Join from './components/Join/Join'
import 'bootstrap/dist/css/bootstrap.min.css';
export default function App() {
    return (
        <BrowserRouter>
            <Route path="/" exact component={Join} />
            <Route path="/chat" component={Chat} />
        </BrowserRouter>
    )
}

