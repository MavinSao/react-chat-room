import React from 'react';
import { InputGroup, FormControl } from 'react-bootstrap';
export default function Input({ message, setMessage, sendMessage }) {
    return (
        <form className="form text-send container" >
            <InputGroup className="mb-3">
                <FormControl
                    value={message}
                    type="text"
                    placeholder="Type a message..."
                    onChange={(event) => setMessage(event.target.value)}
                    onKeyPress={(event) => event.key === 'Enter' ? sendMessage(event) : null}
                />
                <InputGroup.Append>
                    <button type="button" className="btn btn-dark" onClick={(event) => sendMessage(event)}>Send</button>
                </InputGroup.Append>
            </InputGroup>

        </form>
    )
}
