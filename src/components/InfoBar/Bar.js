import React from 'react';
import { Navbar, Nav, NavDropdown } from 'react-bootstrap';
import onlineIcon from '../../icons/onlineIcon.png';
import closeIcon from '../../icons/closeIcon.png';
import { Link } from 'react-router-dom'
export default function Bar({ room, users }) {
    let name = users ? users.map(({ name }) => (
        <div key={name} className="activeItem">
            <Nav.Link>{name}</Nav.Link>
            <img alt="Online Icon" src={onlineIcon} />
        </div>
    )) : null
    console.log(name);

    return (
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <div className="container">
                <Navbar.Brand>{room}<img className="onlineIcon mx-2" src={onlineIcon} alt="online icon" /></Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto text-white">
                        <Nav.Link>Room Members :</Nav.Link>
                        {name}
                    </Nav>
                    <Nav>
                        <Nav.Link eventKey={2} href="/">
                            Leave Room
                    </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </div>
        </Navbar>
    )
}
